import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { 
  MzCardModule,
  MzIconModule,
  MzIconMdiModule,
  MzButtonModule,
  MzInputModule,
  MzProgressModule,
  MzToastModule,
  MzValidationModule,
  MzModalModule, 
  MzCollectionModule
} from 'ngx-materialize'


import { AppComponent } from './app.component';
import { SalarySheetComponent } from './salary-sheet/salary-sheet.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2Webstorage } from 'ngx-webstorage';

@NgModule({
  declarations: [
    AppComponent,
    SalarySheetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MzCardModule,
    MzIconModule,
    MzIconMdiModule,
    MzButtonModule,
    MzInputModule,
    MzProgressModule,
    MzToastModule,
    MzValidationModule,
    MzCollectionModule,
    MzModalModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2Webstorage,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
