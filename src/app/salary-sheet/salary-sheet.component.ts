import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SalaryService } from '../salary.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-salary-sheet',
  templateUrl: './salary-sheet.component.html',
  styleUrls: ['./salary-sheet.component.css']
})
export class SalarySheetComponent implements OnInit {

  salaryForm: FormGroup;
  valid: boolean;
  isSuccess: boolean;
  isFailure: boolean;
  errorMessage: any;
  empExistMessage: any;
  loading: boolean = false;
  empData = []

  validationMessages = {
    'name': {
      'required': 'Employee Name is a Required Field.',
    },
    'basicSalary': {
      'required': 'Basic salary is a Required Field.',
    },
    'allowance': {
      'required': 'allowance is a Required Field.',
    },
    'fine': {
      'required': 'Fine is a Required Field.',
    },
  }

  
  constructor(
    private fb: FormBuilder,
    private salaryService: SalaryService,
    private toastService: MzToastService,
  ) { }

  ngOnInit() {
    this.loadAllEmp()
    this.createForm();
  }

  createForm() {
    this.salaryForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      basicSalary: ['', Validators.required],
      allowance: ['', Validators.required],
      fine: ['', Validators.required]
    });
  }

  submitDetails() {
    if(this.salaryForm.invalid) {
      this.toastService.show('All fields are required', 4000, 'danger');
      return;
    }
    this.salaryService.saveEmployee(this.salaryForm.value).then((response) => {
      this.toastService.show(response.message, 4000, 'success');
      this.resetForm()
      this.loadAllEmp();
    })
    .catch(error => {
      this.toastService.show(error.message, 4000, 'danger');
    });
  }

  loadAllEmp()
  {
    this.salaryService.getAll().then((response) => {
      this.empData = response
    })
    .catch(error => {
      this.empData = []
    });
  }

  editSalary(id){
    for(let emp of this.empData){
      if(emp.id == id){
        this.salaryForm.patchValue(emp);
        return;
      }
    }
    
  }

  getTotal(emp){
    return Number(Number(emp.basicSalary) + Number(emp.allowance)) + Number(emp.fine)
  }

  resetForm(){
    this.salaryForm.reset();
    this.salaryForm.patchValue({id: '', basicSalary: '', allowance: '', fine: ''})
  }

}
