import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SalarySheetComponent } from './salary-sheet/salary-sheet.component';

const routes: Routes = [
    { path: '', redirectTo: '/salary-sheet', pathMatch: 'full' },
    { path: 'salary-sheet', component: SalarySheetComponent }, 
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
