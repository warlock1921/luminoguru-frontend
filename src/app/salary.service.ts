import { Injectable } from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { CommentStmt } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  empData = []
  constructor(
    private localStorageService: LocalStorageService
  ) { }


    /**
     * Save and update user
     * @param data
     */
    public saveEmployee(data): Promise<any> {
      return new Promise((res, rej) => {
        this.empData = this.localStorageService.retrieve('users')
        if(!this.empData){
          data.id = 1
          this.empData = [];
          this.empData.push(data)
          this.localStorageService.store('users', this.empData);
          this.empData = this.localStorageService.retrieve('users')      
          res(this.empData);  
        } else{
          if(data.id){
            for(let i=0; i<= this.empData.length; i++){
              if(this.empData[i].id == data.id){
                this.empData[i] = data;
                break;
              }
            }
            this.localStorageService.store('users', this.empData);   
            res({message: "Salary details updates successfully"});
          } else{
            if(this.checkEmp(data)){
              rej({message: "Employee name is already exists"})
            } else{
              console.log(data);
              let lastObj = this.empData[this.empData.length - 1]
              data.id = lastObj.id + 1;
              this.empData.push(data)
              this.localStorageService.store('users', this.empData);
              this.empData = this.localStorageService.retrieve('users')      
              res({message: "Salary details saved successfully"});
            }
          }
        }
      })
  }

  checkEmp(data){
    const preData = this.localStorageService.retrieve('users');
    for(let emp of preData) {
      if(emp.name.toLowerCase() == data.name.toLowerCase()){
        console.log('yes');
        return true;
      }
    }
    return false;
  }



    /**
     * get  all
     */
    public getAll(): Promise<any> {
      return new Promise((res, rej) => {
        this.empData = this.localStorageService.retrieve('users')
        if(!this.empData){
          rej("No data available");
        } else {
          res(this.empData);
        }
      });
    }

}
